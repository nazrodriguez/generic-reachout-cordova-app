'use strict';

angular.module('myApp.notifications', [])

.controller('NotificationsCtrl', ['$mdSidenav', '$scope', '$rootScope', function($mdSidenav, $scope, $rootScope) {
	$scope.notifications = [
		{
			name: "Promo 1 - 2 x 1"
		}
	];
}]);