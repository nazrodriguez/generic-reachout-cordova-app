'use strict';

angular.module('myApp.detail', [])

.controller('DetailCtrl', ['$mdSidenav', '$scope', '$stateParams', '$rootScope', function($mdSidenav, $scope, $stateParams, $rootScope) {
	$scope.element = angular.fromJson(JSON.parse(atob($stateParams.element)));
	$scope.element.hasMap = false;
	$scope.hasLocation = $scope.element.triggers.hotspot_data;
	$scope.titleFontSize = getFontForTextForDesiredWidth($scope.element.description_title ? $scope.element.description_title : $scope.element.title, $(".info").width());

	console.log($scope.titleFontSize);

	$scope.toggleMap = function(){
		$(".compressable").toggleClass("no-height");
		
		$scope.element.hasMap = !$scope.element.hasMap;

		if($scope.element.hasMap){
			$('.content').animate({
				scrollTop: $(".info").offset().top - $(".toolbar").outerHeight()
			}, 1000, "easeOutQuint");
		}
	}

	/* Setups */
	setTimeout(function(){
		if($scope.hasLocation){
			$scope.element.map = L.map('map').setView([51.505, -0.09], 13);
			L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
			    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			}).addTo($scope.element.map);
			L.marker([51.5, -0.09]).addTo($scope.element.map).bindPopup('A pretty CSS3 popup.<br> Easily customizable.').openPopup();
		}

		$scope.element.swiper = new Swiper (('#swiper-container'), SWIPER_OPTIONS);
	}, 500);
	/* Setups */
}]);