'use strict';

angular.module('myApp.signup', [])

.controller('SignupCtrl', ['$mdSidenav', '$scope', '$rootScope', '$location', '$mdToast', function($mdSidenav, $scope, $rootScope, $location, $mdToast) {

	$scope.signup = function(){
	    
	    var user = {
            name: $scope.name + " " + $scope.sirname,
            first_name: $scope.name,
            last_name: $scope.sirname,
            email: $scope.email,
            password: $scope.password,
            brand: BRAND,
            fields: {
                wantsNews: 'true',//$scope.wantsNews,
                os: 'iOS',
                version: IOSVERSION,
                app_version: VERSION
            },
            national_identity: $scope.id
        };

        Playtomic.Users.signup(user, function(response) {

            if (response.success) {
                console.log(response);
                
                TOKEN = response.token;

                $rootScope.user = response.user.user;
                response.user.user.token = TOKEN;
                $rootScope.user.token = TOKEN;

                $rootScope.isGuest = $rootScope.user.email == HARDCODED_GUEST.mail;

                var query = "INSERT INTO usuario (content, facebook_id, user_id) VALUES (?,?,?)";

                write($rootScope.db, query, [JSON.stringify(response.user.user, null, 4), "", response.user.user.id], function(res) {

                    $rootScope.fillDB();

                }, function(err) {});

            } else {

                if (response.errorcode == "507") {
                    $mdToast.show(
                        $mdToast.simple()
                        .content('Ya existe un usuario con este mail en nuestra base de datos.')
                        .position('top')
                        .hideDelay(3000)
                    );
                } else if(response.exceptionmessage.indexOf("is being used already") != -1){
                    $mdToast.show(
                        $mdToast.simple()
                        .content('El ' + ((response.exceptionmessage.indexOf("dni") != -1) ? 'dni' : 'e-mail') + ' ingresado ya corresponde a otro usuario registrado en la aplicación.')
                        .position('top')
                        .hideDelay(3000)
                    );
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                        .content('Error en el registro.')
                        .position('top')
                        .hideDelay(3000)
                    );
                }
            }
        });
    }
}]);