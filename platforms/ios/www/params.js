var PRIMARY_COLOR = "#D50036",
	SECONDARY_COLOR = "#C3CF3D",
	CONTRAST_COLOR = "#FFFFFF";

var STYLES = {
	toolbar: {
		background: {
			color: PRIMARY_COLOR,
			alpha: 0.4
		},
		color: CONTRAST_COLOR,
		border: {
			top: {
				width: "0px",
				color: CONTRAST_COLOR
			},
			bottom: {
				width: "0px",
				color: PRIMARY_COLOR
			},
			left: {
				width: "0px",
				color: PRIMARY_COLOR
			},
			right: {
				width: "0px",
				color: PRIMARY_COLOR
			}
		},
		height: 65
	},
	page: {
		background: {
			color: "#E0E0E2",
			alpha: 1
		},
		border: {
			top: {
				width: "0px",
				color: "#E0E0E2"
			},
			bottom: {
				width: "0px",
				color: "#E0E0E2"
			},
			left: {
				width: "0px",
				color: "#E0E0E2"
			},
			right: {
				width: "0px",
				color: "#E0E0E2"
			}
		}
	},
	general: {
		bordered: {
			top: {
				width: "6px",
				color: CONTRAST_COLOR
			},
			bottom: {
				width: "6px",
				color: CONTRAST_COLOR
			},
			left: {
				width: "6px",
				color: CONTRAST_COLOR
			},
			right: {
				width: "6px",
				color: CONTRAST_COLOR
			}
		},
		
		selected: {
			icon: {
				color: PRIMARY_COLOR
			}
		},

		contrast: {
			color: CONTRAST_COLOR,
			backgroundColor: PRIMARY_COLOR
		},

		backgroundColor: {
			primary: PRIMARY_COLOR,
			secondary: SECONDARY_COLOR
		},

		color: {
			primary: PRIMARY_COLOR,
			secondary: SECONDARY_COLOR
		}
	}
};