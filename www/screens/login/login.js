'use strict';

angular.module('myApp.login', [])

.controller('LoginCtrl', ['$mdSidenav', '$scope', '$rootScope', '$location', '$mdToast', '$mdDialog', function($mdSidenav, $scope, $rootScope, $location, $mdToast, $mdDialog) {
	
	$scope.forgotPassword = function(ev) {
        var m = $scope.email;
        setTimeout(function() {
            $mdDialog.show({
                locals: {
                    mail: m
                },
                controller: function ($scope, $mdDialog, mail) {
				    $scope.email = mail;

				    $scope.hide = function() {
				        $mdDialog.hide();
				    };

				    $scope.accept = function(ev) {
				        $mdDialog.hide($scope.email);
				    };

				    $scope.cancel = function() {
				        $mdDialog.cancel();
				    };
				},
                controllerAs: 'ctrl',
                template: '<md-dialog style="padding: 20px;"><md-button class="md-icon-button" style="background-image: url(assets/images/cerrar2.png)" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'></md-button><h1 style="font-size: 18px;">Ingresá tu mail para recuperar la contraseña:</h1><div style="overflow-y: scroll" class=""><md-input-container> <label>Email</label><input value="' + $scope.email + '" required type="email" name="email" ng-model="email" minlength="5" maxlength="100" ng-pattern="/^.+@.+\..+$/" /><div ng-messages="projectForm.email.$error" role="alert"><div ng-message-exp="[\'required\', \'minlength\', \'maxlength\', \'pattern\']">Tu email debe ser una dirección válida y ser compuesta por entre 5 y 100 caracteres.</div></div></md-input-container></div><md-button style="width: 100%!important; height: 52px; background-color: black!important; margin: 14px 0px 0px 0px; padding: 0px" ng-click="accept()" class="md-primary md-raised loginBtn"> Aceptar </md-button></md-dialog>',
                clickOutsideToClose: true
            }).then(function(email) {

                if(!email || email == ""){
                    $mdToast.show(
                        $mdToast.simple()
                        .content('Error en el ingreso')
                        .position('top')
                        .hideDelay(3000)
                    );
                    return;
                }

                var c = {
                    email: email,
                    brand: BRAND
                }

                var res = $http.post('http://' + SERVER + '.reachout.global/forgot', JSON.stringify(c));

                res.success(function(data, status, headers, config) {

                    var confirm = $mdDialog.confirm()
                        .title('Reestablecimiento de contraseña')
                        .content(data.success ? 'Te enviamos un link a tu casilla de mail para reestablecer tu contraseña.' : 'El mail ingresado no existe. Intentá nuevamente.')
                        .ariaLabel('Lucky day')
                        .ok('Ok')
                        .targetEvent();

                    $mdDialog.show(confirm).then(function() {
                        $scope.email = email;
                    });
                });

                res.error(function(data, status, headers, config) {
                    console.log(JSON.stringify(data));
                });
            }, function() {});
        }, 500);
    }

    $scope.login = function(e){
        e.preventDefault();

        $rootScope.loading = true;

        var user = {
            email: $scope.email,
            password: $scope.password,
            is_admin: false,
            json: true,
            brand: BRAND
        };

        /*db.transaction(function(tx) {

            tx.executeSql("INSERT OR IGNORE INTO usuario (content, facebook_id, user_id, rDate) VALUES (?,?,?,?)", [JSON.stringify(user, null, 4), "", user.id, user.register_date]);

        }, function(error) {
            console.log("ERROR login.js " + error.message);
        }, function(res) {

            console.log(res);
            console.log("HOLO?");

            $rootScope.user = user;
            $rootScope.user.token = TOKEN;
            $rootScope.dni = user.national_indentity;

            $rootScope.isGuest = $rootScope.user.email == HARDCODED_GUEST.mail;

            $rootScope.fillDB();

        });*/

        Playtomic.Auth.login(user, function(response) {

            if (response.success) {

                TOKEN = response.token;

                response.user.self_user = JSON.stringify(response.user);
                response.user.fields.app_version = VERSION;
                response.user.fields.version = IOSVERSION;
                response.user.fields.os = 'iOS';
                response.user.token = TOKEN;

                Playtomic.Users.update(response.user, function(response) {

                    if (response.user) {
                        console.log(response);

                        var query = "INSERT INTO usuario (content, facebook_id, user_id, rDate) VALUES (?,?,?,?)";
                        write(query, [JSON.stringify(response.user, null, 4), "", response.user.id, response.user.register_date], function(tx, res) {

                            $rootScope.user = response.user;
                            $rootScope.user.token = TOKEN;
                            $rootScope.dni = response.user.national_indentity;

                            $rootScope.isGuest = $rootScope.user.email == HARDCODED_GUEST.mail;

                            $rootScope.fillDB();

                        }, function(tx, res) {
                            console.log("ERROR login.js");
                        });

                        
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                            .content('Error ingresando a tu cuenta. Revisa tu conexión.')
                            .position('top')
                            .hideDelay(5000)
                        );

                        console.log(response);

                        Playtomic.Auth.logout(function(response) {
                            $rootScope.dni = undefined;
                            $rootScope.user = undefined;
                            $rootScope.stars = undefined;
                            deleteTables(createTables);
                        }); 
                    }
                });
            } else {
                $mdToast.show(
                    $mdToast.simple()
                    .content('Error en el ingreso')
                    .position('top')
                    .hideDelay(3000)
                );
            }
        });
    }
}]);