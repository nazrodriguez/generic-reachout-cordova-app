'use strict';

/* Statics */
	var STARTING_ROUTE = "main/home",
		BRAND = "Grisino",
		SWIPER_OPTIONS = {
		    direction: 'horizontal',
		    loop: true,
		    pagination: '.swiper-pagination',
		    nextButton: '.swiper-button-next',
		    prevButton: '.swiper-button-prev'
		},
		LOADER_MIN_TIME = 2,
		LOADER_MAX_TIME = 30;

	var DB_NAME = BRAND + ".db";

	var PRIVATE_KEY = "gd341ui4r67d51srfwvs",
		PUBLIC_KEY = "nkfvt7gpgdkakyb78gr9",
		SERVER = "prod",
		IOSVERSION = "10.3.1",
		VERSION = "0",

		HARDCODED_GUEST = {
	        mail: 'invitado@grisino.com',
	        pass: 'fellowguest'
	    };

	var REQUESTS, TOKEN;
/* Statics */

/* Globals */
	var wow = new WOW({
      boxClass:     'wow',
      animateClass: 'animated',
      offset:       10,
      mobile:       true,
      live:         true
    });

    var encode, db, start;

    var stateInterval,
    	styledDOMDependingRules = false,
    	styledBasicRules = false;
	//
/* Globals */

angular.module('myApp', [
	'ngRoute',
	'ui.router',
	'ngAnimate',
	'ngAria',
	'ngMaterial',
	'ngMessages',
	'myApp.home',
	'myApp.news',
	'myApp.promos',
	'myApp.notifications',
	'myApp.main',
	'myApp.profile',
	'myApp.login',
	'myApp.shops',
	'myApp.signup',
	//'myApp.games',
	'myApp.auth-options',
	'myApp.detail'
])

.config(function($mdThemingProvider) {

	$mdThemingProvider.definePalette(BRAND + "PrimaryPalette", {
		'50': 'd6002d',
		'100': 'd6002d',
		'200': 'd6002d',
		'300': 'd6002d',
		'400': 'd6002d',
		'500': 'd6002d',
		'600': 'd6002d',
		'700': 'd6002d',
		'800': 'd6002d',
		'900': 'd6002d',
		'A100': 'd6002d',
		'A200': 'd6002d',
		'A400': 'd6002d',
		'A700': 'd6002d',
		'contrastDefaultColor': 'light',
		'contrastDarkColors': undefined,
		'contrastLightColors': undefined
	});

  	$mdThemingProvider.definePalette(BRAND + "AccentPalette", {
		'50': '9f6300',
		'100': '9f7600',
		'200': '9f8700',
		'300': '9f9700',
		'400': '819f00',
		'500': '489f00',
		'600': '119f00',
		'700': '009f47',
		'800': '009f9d',
		'900': 'ea9300',
		'A100': 'eac800',
		'A200': 'bfea00',
		'A400': '19ea00',
		'A700': '00ea69',
		'contrastDefaultColor': 'light',
		'contrastDarkColors': ['900', 'A100', 'A200', 'A400', 'A700'],
		'contrastLightColors': undefined
  	});

    $mdThemingProvider.definePalette(BRAND + "WhitePalette", {
		'50': 'ffffff',
		'100': 'ffffff',
		'200': 'ffffff',
		'300': 'ffffff',
		'400': 'ffffff',
		'500': 'ffffff',
		'600': 'ffffff',
		'700': 'ffffff',
		'800': 'ffffff',
		'900': 'ffffff',
		'A100': 'ffffff',
		'A200': 'ffffff',
		'A400': 'ffffff',
		'A700': 'ffffff',
		'contrastDefaultColor': 'light',
		'contrastDarkColors': ['900', 'A100', 'A200', 'A400', 'A700'],
		'contrastLightColors': undefined
  	});

  	$mdThemingProvider.theme('default').primaryPalette(BRAND + "PrimaryPalette").accentPalette(BRAND + "WhitePalette");
})

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  	$stateProvider
    .state('main', {
        url: '/main',
        templateUrl: 'screens/main/main.html',
        controller: 'MainCtrl'
    })
    .state('login', {
        url: '/login',
        templateUrl: 'screens/login/login.html',
        controller: 'LoginCtrl'
    })

	.state('signup', {
		url: '/signup',
		templateUrl: 'screens/signup/signup.html',
		controller: 'SignupCtrl'
	})

	.state('auth-options', {
		url: '/auth-options',
		templateUrl: 'screens/auth-options/auth-options.html',
		controller: 'AuthoptionsCtrl'
	})

	.state('main.home', {
        url: '/home',
        templateUrl: 'screens/home/home.html',
        controller: 'HomeCtrl'
    })

    .state('main.news', {
    	url: '/news',
		templateUrl: 'screens/news/news.html',
		controller: 'NewsCtrl'
	})	

	.state('main.promos', {
		url: '/promos',
		templateUrl: 'screens/promos/promos.html',
		controller: 'PromosCtrl'
	})	

	.state('main.contact', {
		// url: '',
		templateUrl: 'screens/contact/contact.html',
		controller: 'ContactCtrl'
	})

	.state('main.detail', {
		url: '/detail/:element',
		templateUrl: 'screens/detail/detail.html',
		controller: 'DetailCtrl'
	})

	.state('main.games', {
		// url: '',
		templateUrl: 'screens/games/games.html',
		controller: 'GamesCtrl'
	})

	.state('main.notifications', {
		// url: '',
		templateUrl: 'screens/notifications/notifications.html',
		controller: 'NotificationsCtrl'
	})

	.state('main.profile', {
		url: '/profile',
		templateUrl: 'screens/profile/profile.html',
		controller: 'ProfileCtrl'
	})

	.state('main.shops', {
		url: '/shops',
		templateUrl: 'screens/shops/shops.html',
		controller: 'ShopsCtrl'
	});
}])

.run(function($rootScope, $mdSidenav, $mdDialog, $mdToast, $timeout, $location, $interval, $sce, $http){
	/* Initialization */
		Playtomic.initialize(PUBLIC_KEY, PRIVATE_KEY, "http://" + SERVER + ".reachout.global");
		$rootScope.isShowingLogo = false;
		$rootScope.CACHE = {
			promos: [
				{
					title: "2x1 Imperdible",
					description: "No te pierdas los mejores precios sobre la ropa más linda para los chicos! No te pierdas los mejores precios sobre la ropa más linda para los chicos!",
					location: {
						name: "\'Descuento monstruoso de Abril\'"
					},
					images: [
						{
							location: "img/grisino_1.jpg"
						},
						{
							location: "img/grisino_2.jpg"
						}
					],
					map: undefined,
					swiper: undefined,
					index: 0,
					type: "promos"
				},
				{
					title: "2x1 Imperdible",
					description: "No te pierdas los mejores precios sobre la ropa más linda para los chicos! No te pierdas los mejores precios sobre la ropa más linda para los chicos!",
					location: {
						name: "\'Descuento monstruoso de Abril\'"
					},
					images: [
						{
							location: "img/grisino_1.jpg"
						},
						{
							location: "img/grisino_2.jpg"
						}
					],
					map: undefined,
					swiper: undefined,
					index: 1,
					type: "promos"
				},
				{
					title: "2x1 Imperdible",
					description: "No te pierdas los mejores precios sobre la ropa más linda para los chicos! No te pierdas los mejores precios sobre la ropa más linda para los chicos!",
					location: {
						name: "\'Descuento monstruoso de Abril\'"
					},
					images: [
						{
							location: "img/grisino_1.jpg"
						},
						{
							location: "img/grisino_2.jpg"
						}
					],
					map: undefined,
					swiper: undefined,
					index: 2,
					type: "promos"
				},
				{
					title: "2x1 Imperdible",
					description: "No te pierdas los mejores precios sobre la ropa más linda para los chicos! No te pierdas los mejores precios sobre la ropa más linda para los chicos!",
					location: {
						name: "\'Descuento monstruoso de Abril\'"
					},
					images: [
						{
							location: "img/grisino_1.jpg"
						},
						{
							location: "img/grisino_2.jpg"
						}
					],
					map: undefined,
					swiper: undefined,
					index: 3,
					type: "promos"
				}
			],
			news: [
				{
					title: "Abre nuevo local",
					description: "No te pierdas los mejores precios sobre la ropa más linda para los chicos! No te pierdas los mejores precios sobre la ropa más linda para los chicos!",
					location: {
						name: "\'Adrogue\'"
					},
					images: [
						{
							location: "img/grisino_1.jpg"
						},
						{
							location: "img/grisino_2.jpg"
						}
					],
					map: undefined,
					swiper: undefined,
					index: 0,
					type: "news"
				},

				{
					title: "Abre nuevo local!",
					description: "¡No te pierdas los mejores precios sobre la ropa más linda para los chicos! No te pierdas los mejores precios sobre la ropa más linda para los chicos!",
					location: {
						name: "\'Adrogue\'"
					},
					images: [
						{
							location: "img/grisino_1.jpg"
						},
						{
							location: "img/grisino_2.jpg"
						}
					],
					map: undefined,
					swiper: undefined,
					index: 1,
					type: "news"
				}
			],
			shops: [
				{
					title: "Grisino Abasto",
					description: "¡En el shopping con más onda y más color, llegó Grisino para llenarlo aun más de vida!",
					location: {
						name: "\'Villa Crespo\'"
					},
					images: [
						{
							location: "img/grisino_1.jpg"
						},
						{
							location: "img/grisino_2.jpg"
						}
					],
					map: undefined,
					swiper: undefined,
					index: 0,
					type: "shops"
				},

				{
					title: "Grisino Laprida",
					description: "¡En el shopping con más onda y más color, llegó Grisino para llenarlo aun más de vida!",
					location: {
						name: "\'Lomas de Zamora\'"
					},
					images: [
						{
							location: "img/grisino_1.jpg"
						},
						{
							location: "img/grisino_2.jpg"
						}
					],
					map: undefined,
					swiper: undefined,
					index: 1,
					type: "shops"
				},

				{
					title: "Grisino Adrogué",
					description: "¡En el shopping con más onda y más color, llegó Grisino para llenarlo aun más de vida!",
					location: {
						name: "\'Adrogué\'"
					},
					images: [
						{
							location: "img/grisino_1.jpg"
						},
						{
							location: "img/grisino_2.jpg"
						}
					],
					map: undefined,
					swiper: undefined,
					index: 2,
					type: "shops"
				}
			]
		}
	/* Initialization */
	
	/* Navigation */
		$rootScope.toggleSidenav = function() {
			$mdSidenav('left').toggle();	
	    }
	    $rootScope.go = function(path, params){  
	    	if(params){
	    		var s = path + "/" + btoa(JSON.stringify(params));
	    		$location.path(s);	
	    	}
	    	else
	    		$location.path(path);
	    }

	    $rootScope.browser = function ( link ){
			window.open(link, '_blank');
		};

	    /*Upon wanting to change state*/
		    $rootScope.$on('$stateChangeStart', function(event, next, nextParams, current, currentParams) {
		    	$rootScope.isShowingLogo = false;
		    	
		    	if(stateInterval)
		    		$interval.cancel(stateInterval);
		    	
		    	stateInterval = $interval(function(){	
					$rootScope.isShowingLogo = !$rootScope.isShowingLogo;
				}, 5000);
		    });
    	/*Upon wanting to change state*/

    	/*Upon having successfully changed state*/
	        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
	            $rootScope.currentPath = $location.path();

	            if(toState.name.indexOf("main") != -1 && !styledDOMDependingRules){
	            	styleBasicRules();
	            	styleDOMDependingRules();
	            	styledBasicRules = true;
	            	styledDOMDependingRules = true;
	            }

	            if(toState.name.indexOf("main") == -1 && !styledBasicRules){
	            	styleBasicRules();
	            	styledBasicRules = true;
	            }

	            switch (toState.name) {
	                case "main.promos":
	                	$rootScope.toolbarIcon = $sce.trustAsHtml('<i class="flaticon-fix flaticon-tag"></i>');
	                    $rootScope.state = "Promociones";
	                    break;
	                case "main.home":
	                	$rootScope.toolbarIcon = $sce.trustAsHtml('<i class="flaticon-fix flaticon-bookmark"></i>');
	                    $rootScope.state = "Destacado";
	                    break;
	                case "main.detail":
	                	$rootScope.toolbarIcon = $sce.trustAsHtml('');
	                    $rootScope.state = "Detalle";
	                    break;
	                case "main.contacto":
	                	$rootScope.toolbarIcon = $sce.trustAsHtml('<i class="flaticon-fix flaticon-lightining"></i>');
	                    $rootScope.state = "Contacto";
	                    break;
	                case "main.shops":
	                	$rootScope.toolbarIcon = $sce.trustAsHtml('<i class="flaticon-fix flaticon-pin"></i>');
	                    $rootScope.state = "Locales";
	                    break;
	                case "main.news":
	                	$rootScope.toolbarIcon = $sce.trustAsHtml('<i class="flaticon-fix flaticon-book"></i>');
	                    $rootScope.state = "Lo nuevo!";
	                    break;
	                case "main.games":
	                	$rootScope.toolbarIcon = $sce.trustAsHtml('<i class="flaticon-fix flaticon-smartphone"></i>');
	                    $rootScope.state = "¡Juegos!";
	                    break;
	                case "main.profile":
	                	$rootScope.toolbarIcon = $sce.trustAsHtml('<i class="flaticon-fix flaticon-user"></i>');
	                    $rootScope.state = "Perfil";
	                    break;
	                case "main.notifications":
	                	$rootScope.toolbarIcon = $sce.trustAsHtml('<i class="flaticon-fix flaticon-flag"></i>');
	                    $rootScope.state = "Notificaciones";
	                    break;
	            }
	        });
        /*Upon having successfully changed state*/
    /* Navigation */

    /* Detail */
    	$rootScope.detail = function(type, id){
    		var index;

    		type = type == "promos" ? type : "news";

    		for(var i = 0; i < $rootScope.CACHE[type].length; i++)
			if($rootScope.CACHE[type][i].id == id){
				index = i;
				break;
			}

			if(!index)
				return;

	    	$rootScope.CACHE[type][index].swiper = undefined;
	    	$rootScope.CACHE[type][index].map = undefined;
	    	$rootScope.go("/main/detail", angular.toJson($rootScope.CACHE[type][index]));
	    }
    /* Detail */    

    /* Loading */
    	$rootScope.loader = new Loader();
    /* Loading */

    /* DB Handling */
    	$rootScope.fillDB = function(){
    		read("contenidos", function(res) {
                if (res.rows.length > 0) {
                    $rootScope.CACHE.news = JSON.parse(res.rows.item(res.rows.length - 1).content);

                    console.log("Getting content from CACHE");
                    $rootScope.getContentFromCache();
                } else {

                	console.log("Getting content from server");
                    $rootScope.downloadContent();
                }

                console.log($rootScope.CACHE);

                $rootScope.go('/main/home');
            }, function(err) {	
            	console.log("ERROR");
            	$rootScope.go('/auth-options');
            });
    	}

    	$rootScope.downloadContent = function(){
    		downloadNews().then((elements) => {
                write("INSERT INTO contenidos (fecha, content) VALUES (?,?)", [elements[0].register_date, JSON.stringify(elements, null, 4)], function(res) {
                	$rootScope.CACHE.news = elements;
                }, function(err) {
                	console.log("News download´s DB save error: " + err);
                });
			}).catch((error) => {
				console.log(error);
	            $mdToast.show(
                    $mdToast.simple()
                    .content('Error, no se han encontrado contenidos. ¡Intenta más tarde!')
                    .position('top')
                    .hideDelay(3000)
                );
                $rootScope.eraseDB( true );
                $rootScope.go("/auth-options");
	        });

    		downloadPromos().then((elements) => {
    			//BROKEN SHOPS WORKAROUND
	            elements = elements.filter(function(promo){
	                if (promo.audience.shops.length > 0) {
	                    var hasAtLeastOne = false;
	                    for (var j = 0; j < promo.audience.shops.length; j++) {
	                        if(promo.audience.shops[j].map.lat && promo.audience.shops[j].map.lat != null){
	                            hasAtLeastOne = true;
	                            break;
	                        }
	                    }
	                    return hasAtLeastOne;
	                } else {
	                    return true;
	                }
	            });
	            //BROKEN SHOPS WORKAROUND

                write("INSERT INTO promos (fecha, content) VALUES (?,?)", [elements[0].register_date, JSON.stringify(elements, null, 4)], function(res) {
                    $rootScope.CACHE.promos = elements;
                }, function(err) {
                	console.log("Promos download´s DB save error: " + err);
                });
    		}).catch((error) => {
    			console.log(error);
    			$mdToast.show(
                    $mdToast.simple()
                    .content('Error, no se han encontrado promociones. ¡Intenta más tarde!')
                    .position('top')
                    .hideDelay(3000)
                );
                $rootScope.eraseDB( true );
                $rootScope.go("/auth-options");
    		});

    		downloadCategories($rootScope.user, $http).then((elements) => {
    			write("INSERT INTO categorias (fecha, content) VALUES (?,?)", ["", JSON.stringify(elements, null, 4)], function(res) {
	                $rootScope.CACHE.categories = elements;
	            }, function(err) {
	            	console.log("Categories download´s DB save error: " + err);
	            });
    		}).catch((error) => {
    			console.log(error);
    			//
    		});

    		downloadShops().then((elements) => {
                write("INSERT INTO venues (fecha, content) VALUES (?,?)", [elements[0].register_date, JSON.stringify(elements, null, 4)], function(res) {
                	$rootScope.CACHE.shops = elements;
                }, function(err) {});    
    		}).catch((error) => {
    			console.log(error);
    			//
    		});
    	}

    	$rootScope.getContentFromCache = function(){
            read("contenidos", function(res) {
                if (res.rows.length != 0) {
                    $rootScope.CACHE.news = JSON.parse(res.rows.item(res.rows.length - 1).content);
                    cachePromos();
                }
            }, function(err) {});

            read("promos", function(res) {
                if (res.rows.length != 0) {
                    $rootScope.CACHE.promos = JSON.parse(res.rows.item(res.rows.length - 1).content);
                    cacheVenues();
                }
            }, function(err) {});

            read("venues", function(res) {
                if (res.rows.length != 0) {
                    $rootScope.CACHE.shops = JSON.parse(res.rows.item(res.rows.length - 1).content);
                    cacheCategorias();
                }
            }, function(err) {});

            read("categorias", function(res) {
                if (res.rows.length != 0) {
                    $rootScope.CACHE.categories = JSON.parse(res.rows.item( res.rows.length - 1 ).content);
                }
            }, function(err) {});
    	}
    /* DB Handling */

    /* Authentication */
    	$rootScope.facebookLogin = function(){

	        $rootScope.loader.toggle();
	        facebookConnectPlugin.login(["public_profile", "user_birthday", "user_friends", "email"], function(success) {

	        	console.log(success);

	            var facebookID = success.authResponse.userID;
	            var user = {
	                //email: success.email,
	                //gender: success.gender,
	                facebookId: facebookID,
	                accessToken: success.authResponse.accessToken,
	                brand: BRAND,
	                fields: JSON.stringify({
	                    wantsNews: true,
	                    os: 'iOS',
	                    version: IOSVERSION,
	                    app_version: VERSION
	                })
	            };

	            Playtomic.Auth.loginFacebook(user, function(response) {
	                if (response.success) {

	                    $rootScope.isGuest = false;

	                    if (response.user.national_identity != '') {

	                        TOKEN = response.token;
	                        $rootScope.user = response.user;
	                        $rootScope.user.token = TOKEN;

	                        var query = "INSERT INTO usuario (content, facebook_id, user_id, rDate) VALUES (?,?,?,?)";
	                        write(query, [JSON.stringify(response.user, null, 4), facebookID, response.user.id, response.user.register_date], function(res) {}, function(err) {});

	                        // MAJOR TODO Ask for stars
	                        /*$rootScope.getStars(function(){
	                            $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);    
	                        });*/

	                        $rootScope.user.facebookId = facebookID;
	                        $rootScope.user.facebookUserImage = "https://graph.facebook.com/" + $rootScope.user.facebookId + "/picture?width=100&height=100";
	                        $rootScope.user.facebookUserImageBig = "https://graph.facebook.com/" + $rootScope.user.facebookId + "/picture?width=" + $(document).width();
	                        
	                        $rootScope.fillDB();

	                    } else {

	                        // ask for dni and for stars later, then save it in sqlite
	                        $rootScope.askDNI(function() {
	                            $rootScope.loading = true;

	                            /*$rootScope.getStars(function(){
	                                $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);    
	                            });*/

	                            TOKEN = response.token;
	                            var user = response.user;
	                            user.self_user = JSON.stringify(response.user);
	                            user.national_identity = $rootScope.dni;
	                            user.token = TOKEN;
	                            user.facebookId = facebookID;
	                            user.facebookUserImage = "https://graph.facebook.com/" + facebookID + "/picture?width=100&height=100";
	                            user.facebookUserImageBig = "https://graph.facebook.com/" + facebookID + "/picture?width=" + $("#mainView").width();

	                            //Update user in our db
	                            Playtomic.Users.update(user, function(response) {
	                                //Update success     
	                                if (response.user) {

	                                    $rootScope.user = user;

	                                    //Successfully updated user to save his DNI
	                                    var query = "INSERT INTO usuario (content, facebook_id, user_id, rDate) VALUES (?,?,?,?)";
	                                    write(query, [JSON.stringify(response.user, null, 4), facebookID, response.user.id, response.user.register_date], function(res) {

	                                        $rootScope.fillDB();

	                                    }, function(err) {});
	                                } else {

                                        Playtomic.Auth.logout(function(response) {
                                            $rootScope.dni = undefined;
                                            $rootScope.user = undefined;
                                            $rootScope.stars = undefined;
                                            deleteTables(createTables);
                                            $rootScope.loading = false;
                                            $rootScope.go("/auth-options");
                                            $mdToast.show(
                                                $mdToast.simple()
                                                .content('No se pudo guardar el DNI. Es posible que hubiera un fallo de conexión. Reintentalo.')
                                                .position('top')
                                                .hideDelay(3000)
                                            );
                                        });
	                                    
	                                }
	                            });
	                        });
	                    }
	                } else {
	                    console.log(response);
	                    $rootScope.loading = false;

	                    $mdDialog.show($mdDialog.confirm().title('Aviso').content('Tu cuenta de facebook no tiene un email válido o no es visible. Por favor, utiliza la opción "Registrate", para registrarte por email.').ariaLabel('Lucky day').ok('Aceptar').targetEvent()).then(function() {}, function() {});

	                    $('#alertField').css("display", "inherit");
	                }
	            });

	        }, function(error) {
	            console.log("Error logging in with Facebook. " + JSON.stringify(error));
	        });
    	}

    	$rootScope.logOut = function(){
	        setTimeout(function() {
	            var confirm = $mdDialog.confirm()
	            .title('Cerrar sesión y salir')
	            .content('¿Seguro que desea cerrar la sesión? Se cerraría la aplicación.')
	            .ariaLabel('Lucky day')
	            .ok('Cerrar sesión')
	            .cancel('Volver');
	            $mdDialog.show(confirm).then(function() {

	                if ($rootScope.user.facebookId != "") 
	                    facebookConnectPlugin.logout();
	                
                    Playtomic.Auth.logout(function(response) {
                        $rootScope.dni = undefined;
                        $rootScope.user = undefined;
                        $rootScope.stars = undefined;
                        deleteTables(createTables);
                    });
	                
	            }, function() {});
	        }, 500);
    	}
    /* Authentication */

	/* Preventive auth-options upbringing */
    $rootScope.go("/auth-options");

    /* If found a user in DB, cache content and head home, otherwise show auth options */
	    start = function(db){
	    	createTables(function(){
	    		console.log("Create tables success.");

	    		read("usuario", function(res) {
			        if (res.rows.length > 0) {

			            $rootScope.user = JSON.parse(res.rows.item(res.rows.length - 1).content);
			            $rootScope.dni = $rootScope.user.national_identity;
			            $rootScope.isGuest = $rootScope.user.email == HARDCODED_GUEST.mail;

			            TOKEN = $rootScope.user.token;

			            if($rootScope.dni && $rootScope.dni != ""){
			                $rootScope.getContentFromCache();
			                $rootScope.go('/main/home');
			            } else {
			                $rootScope.go('/auth-options');    
			            }
			                                      
			        } else {
			        	console.log("No user detected.");
			            $rootScope.go('/auth-options');
			        }
			    }, function(err) {
			    	console.log(err);
			    });
	    	});
	    }
 	/* If found a user in DB, cache content and head home, otherwise show auth options */

});

document.addEventListener('deviceready', function(){
	db = window.sqlitePlugin.openDatabase({name: DB_NAME, location: "default"});
	console.log("DEVICEREADY");
	setTimeout(start, 5000);
	//start(db);
});
