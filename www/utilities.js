/* SQLite */
	function write(query, data, success, failure) {
	    db.transaction(function(tx) {
	        tx.executeSql(query, data);
	    }, function(err){
            console.log("Error writing thru " + query + ".");
            if(failure)
                failure(tx, err);
        }, success);
	}

	function read(table, success, failure){

        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM ' + table, [], function(tx, rs) {
                if(success)
                    success(rs);
            }, function(tx, error) {
                console.log("read", "Error reading. " + error.message);
                if(failure)
                    failure(error);
            });
        });
    }

    function createTable(t) {
        var query = "CREATE TABLE IF NOT EXISTS " + t.table;

        db.transaction(function(tx) {
            console.log(query);
            tx.executeSql(query);
        }, function (err) {
            console.log("createTable", "Error creating table " + t.table + ". '" + err.message + "'.")
        }, function (res) {
            console.log(res);
            if (t.callback)
                t.callback();
        });
    }
    
    function deleteTable(t) {
        var query = "DROP TABLE IF EXISTS " + t.table;

        db.transaction(function(tx) {
            tx.executeSql(query);
        }, function (err) {
            say("deleteTable", "Error dropping table " + t.table + ". '" + err.message + "'.");
            return true;
        }, function () {
            if (t.callback)
                t.callback();
        });
    }

    function createTables(callback) {
        createTable({
            table: "logs (_id INTEGER NOT NULL PRIMARY KEY, log TEXT)",
            callback: function() {
                createTable({
                    table: "usuario (_id INTEGER NOT NULL PRIMARY KEY, content TEXT, facebook_id TEXT, user_id TEXT, rDate TEXT)",
                    callback: function() {
                        createTable({
                            table: "contenidos (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , content TEXT )",
                            callback: function() {
                                createTable({
                                    table: "promos (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , content TEXT )",
                                    callback: function() {
                                        createTable({
                                            table: "venues (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , content TEXT )",
                                            callback: function() {
                                                createTable({
                                                    table: "categorias (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , content TEXT )",
                                                    callback: function() {
                                                        createTable({
                                                            table: "notificaciones (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , notificacionId TEXT, elementId TEXT, geofence TEXT)",
                                                            callback: function() {
                                                                createTable({
                                                                    table: "config (_id INTEGER NOT NULL PRIMARY KEY, content TEXT)",
                                                                    callback: function() {
                                                                        if (callback)
                                                                            callback();
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    function deleteTables(callback) {
        deleteTable({
            table: "logs",
            callback: function() {
                deleteTable({
                    table: "usuario",
                    callback: function() {
                        deleteTable({
                            table: "contenidos",
                            callback: function() {
                                deleteTable({
                                    table: "promos",
                                    callback: function() {
                                        deleteTable({
                                            table: "venues",
                                            callback: function() {
                                                deleteTable({
                                                    table: "categorias",
                                                    callback: function() {                                                
                                                        deleteTable({
                                                            table: "config",
                                                            callback: function() {
                                                                console.log("Success! - CreateTables");

                                                                if (callback)
                                                                    callback();
                                                            }
                                                        });                                                 
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
/* SQLite */

/* Download content */
    function downloadNews() {
    	return new Promise((resolve, reject) => {
    		Playtomic.Contents.list({type:'',category:'', is_admin: false}, function(response) {
	            if(response.contents && response.contents.length)
	            	resolve(response.contents);
	            else
	            	reject("No content found.");
	        });
		});
    }

    function downloadPromos() {
    	return new Promise((resolve, reject) => {
	        Playtomic.Promos.list({is_admin: false}, function(response) {
	            if(response.promos && response.promos.length)
	            	resolve(response.promos);
	           	else
	           		reject("No promos found.");
	        });
	    });
    }

    function downloadCategories(user, http) {
    	return new Promise((resolve, reject) => {
	        if(!user._id && !user.id)
	        	reject("No categories found.");

	        var data = {
	        	data: REQUESTS.categories.data,
	        	hash: REQUESTS.categories.hash,
	        	userId: (user._id ? user._id : user.id)
	        };

	        var res = http.post('http://' + SERVER + '.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9', data, {headers: {'Authorization':'Bearer ' + TOKEN}});
	        res.success(function(data, status, headers, config) {
	            resolve(data);
	        });

	        res.error(function(data, status, headers, config) {
	            reject("Could not download categories");   
	        });
	    });
    }

    function downloadShops() {
    	return new Promise((resolve, reject) => {
	        Playtomic.Shops.list({}, function(response) {
	        	if(response.shops.shops && response.shops.shops.length)
	            	resolve(response.shops.shops);
	            else
	            	reject("No shops found.");
	        });
	    });
    }
/* Download content */

/* Dynamic styles*/
	var appcss = getStylesheetByHref("app.css"),
		fixescss = getStylesheetByHref("fixes.css");

	function styleDOMDependingRules(){
		setTimeout(function(){
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar", "color", STYLES.toolbar.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar", "backgroundColor", STYLES.toolbar.background.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar", "borderTop", STYLES.toolbar.border.top.width + " solid " + STYLES.toolbar.border.top.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar", "borderBottom", STYLES.toolbar.border.bottom.width + " solid " + STYLES.toolbar.border.bottom.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar", "borderLeft", STYLES.toolbar.border.left.width + " solid " + STYLES.toolbar.border.left.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar", "borderRight", STYLES.toolbar.border.right.width + " solid " + STYLES.toolbar.border.right.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar", "width", (parseInt(document.getElementsByClassName("toolbar")[0].clientWidth) - parseInt(STYLES.toolbar.border.left.width) - parseInt(STYLES.toolbar.border.right.width)) + "px");
		
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar", "margin-top", "20px");
		
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar", "height", (parseInt(STYLES.toolbar.height) + parseInt(STYLES.toolbar.border.top.width) + parseInt(STYLES.toolbar.border.bottom.width)) + "px");

		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar > .wrapper", "height", (parseInt(STYLES.toolbar.height) + parseInt(STYLES.toolbar.border.top.width) + parseInt(STYLES.toolbar.border.bottom.width)) + "px");
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar > .wrapper", "width", (document.getElementsByClassName("toolbar")[0].clientWidth));
		
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar > button > i::before", "color", STYLES.toolbar.color);
		var calculatedMargin = (parseInt(STYLES.toolbar.height) + parseInt(STYLES.toolbar.border.top.width) + parseInt(STYLES.toolbar.border.bottom.width) - ($(".toolbar > button").height())) / 2;
		changeStyleFromRuleAtStylesheetToValue(appcss, ".toolbar > button", "margin-top", (calculatedMargin > 0 ? calculatedMargin : 0) + "px");

		changeStyleFromRuleAtStylesheetToValue(appcss, ".page", "margin-top", (parseInt(STYLES.toolbar.height) + parseInt(STYLES.toolbar.border.top.width) + parseInt(STYLES.toolbar.border.bottom.width) + 20) + "px");
		changeStyleFromRuleAtStylesheetToValue(appcss, ".page", "height", "calc(100% - " + (parseInt(STYLES.toolbar.height) + parseInt(STYLES.toolbar.border.top.width) + parseInt(STYLES.toolbar.border.bottom.width) + 20) + "px)");
		
		// View Container
		changeStyleFromRuleAtStylesheetToValue(appcss, ".offset.view-container", "backgroundColor", STYLES.page.background.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".offset.view-container", "borderTop", STYLES.page.border.top.width + " solid " + STYLES.page.border.top.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".offset.view-container", "borderBottom", STYLES.page.border.bottom.width + " solid " + STYLES.page.border.bottom.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".offset.view-container", "borderLeft", STYLES.page.border.left.width + " solid " + STYLES.page.border.left.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".offset.view-container", "borderRight", STYLES.page.border.right.width + " solid " + STYLES.page.border.right.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".offset.view-container", "width", (parseInt(document.getElementById("view-container").clientWidth) - parseInt(STYLES.page.border.left.width) - parseInt(STYLES.page.border.right.width)) + "px");	
		//changeStyleFromRuleAtStylesheetToValue(appcss, ".offset.view-container", "height", ($(".offset.view-container").height() - ($(".toolbar").innerHeight() + parseInt(STYLES.toolbar.border.bottom.width) + parseInt(STYLES.toolbar.border.top.width) + parseInt(STYLES.page.border.top.width) + parseInt(STYLES.page.border.bottom.width))) + "px");
		
		//changeStyleFromRuleAtStylesheetToValue(appcss, ".offset.view-container", "marginTop", ($(".toolbar").innerHeight() + /*parseInt($(".toolbar").css("margin-top")) +*/ parseInt(STYLES.toolbar.border.bottom.width) + parseInt(STYLES.toolbar.border.top.width)) + "px");
		changeStyleFromRuleAtStylesheetToValue(fixescss, "md-sidenav > md-content", "height", ($("md-sidenav").height() - 200) + "px");
		changeStyleFromRuleAtStylesheetToValue(fixescss, "md-sidenav > md-content", "marginTop", "200px");
		}, 500);
	}

	function styleBasicRules(){	
		changeStyleFromRuleAtStylesheetToValue(appcss, ".facebook.background > .dummy", "backgroundImage", "url(img/fb_test.jpg)");	

		// All bordered
		changeStyleFromRuleAtStylesheetToValue(appcss, ".bordered", "borderTop", STYLES.general.bordered.top.width + " solid " + STYLES.general.bordered.top.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".bordered", "borderBottom", STYLES.general.bordered.bottom.width + " solid " + STYLES.general.bordered.bottom.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".bordered", "borderLeft", STYLES.general.bordered.left.width + " solid " + STYLES.general.bordered.left.color);
		changeStyleFromRuleAtStylesheetToValue(appcss, ".bordered", "borderRight", STYLES.general.bordered.right.width + " solid " + STYLES.general.bordered.right.color);

		changeStyleFromRuleAtStylesheetToValue(appcss, "i.selected::before", "color", STYLES.general.selected.icon.color);

		// All contrasted
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".contrast", "color", STYLES.general.contrast.color);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".contrast", "borderColor", STYLES.general.contrast.color);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".contrast", "backgroundColor", STYLES.general.contrast.backgroundColor);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".counter.contrast", "color", STYLES.general.contrast.backgroundColor);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".contrast", "borderColor", STYLES.general.contrast.backgroundColor);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".counter.contrast", "backgroundColor", STYLES.general.contrast.color);

		changeStyleFromRuleAtStylesheetToValue(fixescss, ".background.color:not(.secondary)", "backgroundColor", STYLES.general.backgroundColor.primary);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".background.color.secondary", "backgroundColor", STYLES.general.backgroundColor.secondary);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".text.color:not(.secondary)", "color", STYLES.general.color.primary);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".text.color.secondary", "color", STYLES.general.color.secondary);

		changeStyleFromRuleAtStylesheetToValue(fixescss, ".no-hover-change.contrast.counter.background.md-button.md-fab:not([disabled]):hover", "backgroundColor", STYLES.general.contrast.color);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".no-hover-change.contrast.background.md-button.md-fab:not(.counter):not([disabled]):hover", "backgroundColor", STYLES.general.contrast.backgroundColor);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".no-hover-change.primary.background.md-button.md-fab:not(.secondary):not([disabled]):hover", "backgroundColor", STYLES.general.backgroundColor.primary);
		changeStyleFromRuleAtStylesheetToValue(fixescss, ".no-hover-change.secondary.background.md-button.md-fab:not(.primary):not([disabled]):hover", "backgroundColor", STYLES.general.backgroundColor.secondary);
	}
/* Dynamic styles*/

/* Utilities */
    function getFontForTextForDesiredWidth(text, desiredWidth){
        var test = $(".textTest");

        test.html(text);
        test.css("font-size", "1px");
        
        var currentFontSize = parseInt(test.css("font-size")),
            currentWidth = parseInt(test.css("width"));

        return ((currentFontSize * desiredWidth) / currentWidth) + "px";
    }

	function changeStyleFromRuleAtStylesheetToValue(styleSheet, rule, style, value, important){		
		var searchingRule;
		for(var i = 0; i < styleSheet.rules.length; i++){
			searchingRule = styleSheet.rules[i];
			if(searchingRule.selectorText === rule){
				
				searchingRule.style[style] = value;
				break;
			}
		}
	}

	function getStylesheetByHref(href){
		var s;
		
		for(var i = 0; i < document.styleSheets.length; i++){
			if(document.styleSheets[i].href && document.styleSheets[i].href.indexOf(href) != -1){
				s = document.styleSheets[i];
				break;
			}
		}

		return s;
	}

	function setupElement( element ){
        if(!element.swiper){
            setTimeout(function(){
                element.swiper = new Swiper (('#swiper-container-' + element.index), SWIPER_OPTIONS);
            }, 500);         
        }
    }	
/* Utilities */

/* Loader */
	function Loader(){
		var jobj = $(".loader"),
			isUp = false,
			time = {
				min: undefined,
				max: undefined,
				passed: 0
			},
			block = false;

		function resetTime(){
			clearTimeout(time.min);
			clearTimeout(time.max);
			clearInterval(time.ongoing);
			time.passed = 0;
		}

		this.toggle = function(){
			isUp = !isUp;

			if(isUp){
				console.log("Wants to be up");
				if(!block){
					console.log("Block false");
					
					// Block until minimum time has passed
					block = true;

					// Track time
					time.ongoing = setInterval(function(){
						time.passed += 1;
					}, 1000);

					// Prevent to little a time
					time.min = setTimeout(function(){
						block = false;
					}, LOADER_MIN_TIME * 1000);

					// Prevent too much time
					time.max = setTimeout(function(){
						block = true;
						isUp = !isUp;

						resetTime();

						jobj.animate({ opacity: 0 }, 500, function(){
							block = false;
							jobj.css("pointer-events", "none");
						});
					}, LOADER_MAX_TIME * 1000);

					// Has activated
					jobj.animate({ opacity: 1 }, 500, function(){ jobj.css("pointer-events", "all"); });
				}
			} else {
				console.log("Wants to close");

				if(block){
					console.log("block true");
					isUp = !isUp;

					setTimeout(function(){
						isUp = !isUp;
						block = false;
						jobj.animate({ opacity: 0 }, 500, function(){ jobj.css("pointer-events", "none"); });
					}, (LOADER_MIN_TIME - time.passed) * 1000);
				} else {
					console.log("block false");
					jobj.animate({ opacity: 0 }, 500, function(){ resetTime(); jobj.css("pointer-events", "none"); });
				}	
			}
		}    		
	}
/* Loader */

/* Encoder */
	function Encoder() {
		var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
		var hex_chr = "0123456789abcdef";

		this.base64 = function(str) {
		    var output = "";
		    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		    var i = 0;

		    str = _utf8_encode(str);
		
		    while (i < str.length) {
		        chr1 = str.charCodeAt(i++);
		        chr2 = str.charCodeAt(i++);
		        chr3 = str.charCodeAt(i++);
		        enc1 = chr1 >> 2;
		        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
		        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
		        enc4 = chr3 & 63;
		
		        if (isNaN(chr2)) { 
		            enc3 = enc4 = 64;
		        } else if (isNaN(chr3)) {
		            enc4 = 64;
				}

		        output = output + _keyStr.charAt(enc1) + _keyStr.charAt(enc2) + _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
		    }
		
		    return output;
		},

		this.md5 = function(str) {
			var x = str2blks_MD5(str);
			var a =  1732584193;
			var b = -271733879;
			var c = -1732584194;
			var d =  271733878;

			for(var i=0; i<x.length; i += 16) {
				var olda = a;
				var oldb = b;
				var oldc = c;
				var oldd = d;
	
				a = ff(a, b, c, d, x[i+ 0], 7 , -680876936);
				d = ff(d, a, b, c, x[i+ 1], 12, -389564586);
				c = ff(c, d, a, b, x[i+ 2], 17,  606105819);
				b = ff(b, c, d, a, x[i+ 3], 22, -1044525330);
				a = ff(a, b, c, d, x[i+ 4], 7 , -176418897);
				d = ff(d, a, b, c, x[i+ 5], 12,  1200080426);
				c = ff(c, d, a, b, x[i+ 6], 17, -1473231341);
				b = ff(b, c, d, a, x[i+ 7], 22, -45705983);
				a = ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
				d = ff(d, a, b, c, x[i+ 9], 12, -1958414417);
				c = ff(c, d, a, b, x[i+10], 17, -42063);
				b = ff(b, c, d, a, x[i+11], 22, -1990404162);
				a = ff(a, b, c, d, x[i+12], 7 ,  1804603682);
				d = ff(d, a, b, c, x[i+13], 12, -40341101);
				c = ff(c, d, a, b, x[i+14], 17, -1502002290);
				b = ff(b, c, d, a, x[i+15], 22,  1236535329);    
				a = gg(a, b, c, d, x[i+ 1], 5 , -165796510);
				d = gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
				c = gg(c, d, a, b, x[i+11], 14,  643717713);
				b = gg(b, c, d, a, x[i+ 0], 20, -373897302);
				a = gg(a, b, c, d, x[i+ 5], 5 , -701558691);
				d = gg(d, a, b, c, x[i+10], 9 ,  38016083);
				c = gg(c, d, a, b, x[i+15], 14, -660478335);
				b = gg(b, c, d, a, x[i+ 4], 20, -405537848);
				a = gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
				d = gg(d, a, b, c, x[i+14], 9 , -1019803690);
				c = gg(c, d, a, b, x[i+ 3], 14, -187363961);
				b = gg(b, c, d, a, x[i+ 8], 20,  1163531501);
				a = gg(a, b, c, d, x[i+13], 5 , -1444681467);
				d = gg(d, a, b, c, x[i+ 2], 9 , -51403784);
				c = gg(c, d, a, b, x[i+ 7], 14,  1735328473);
				b = gg(b, c, d, a, x[i+12], 20, -1926607734);
				a = hh(a, b, c, d, x[i+ 5], 4 , -378558);
				d = hh(d, a, b, c, x[i+ 8], 11, -2022574463);
				c = hh(c, d, a, b, x[i+11], 16,  1839030562);
				b = hh(b, c, d, a, x[i+14], 23, -35309556);
				a = hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
				d = hh(d, a, b, c, x[i+ 4], 11,  1272893353);
				c = hh(c, d, a, b, x[i+ 7], 16, -155497632);
				b = hh(b, c, d, a, x[i+10], 23, -1094730640);
				a = hh(a, b, c, d, x[i+13], 4 ,  681279174);
				d = hh(d, a, b, c, x[i+ 0], 11, -358537222);
				c = hh(c, d, a, b, x[i+ 3], 16, -722521979);
				b = hh(b, c, d, a, x[i+ 6], 23,  76029189);
				a = hh(a, b, c, d, x[i+ 9], 4 , -640364487);
				d = hh(d, a, b, c, x[i+12], 11, -421815835);
				c = hh(c, d, a, b, x[i+15], 16,  530742520);
				b = hh(b, c, d, a, x[i+ 2], 23, -995338651);
				a = ii(a, b, c, d, x[i+ 0], 6 , -198630844);
				d = ii(d, a, b, c, x[i+ 7], 10,  1126891415);
				c = ii(c, d, a, b, x[i+14], 15, -1416354905);
				b = ii(b, c, d, a, x[i+ 5], 21, -57434055);
				a = ii(a, b, c, d, x[i+12], 6 ,  1700485571);
				d = ii(d, a, b, c, x[i+ 3], 10, -1894986606);
				c = ii(c, d, a, b, x[i+10], 15, -1051523);
				b = ii(b, c, d, a, x[i+ 1], 21, -2054922799);
				a = ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
				d = ii(d, a, b, c, x[i+15], 10, -30611744);
				c = ii(c, d, a, b, x[i+ 6], 15, -1560198380);
				b = ii(b, c, d, a, x[i+13], 21,  1309151649);
				a = ii(a, b, c, d, x[i+ 4], 6 , -145523070);
				d = ii(d, a, b, c, x[i+11], 10, -1120210379);
				c = ii(c, d, a, b, x[i+ 2], 15,  718787259);
				b = ii(b, c, d, a, x[i+ 9], 21, -343485551);
	
				a = addme(a, olda);
				b = addme(b, oldb);
				c = addme(c, oldc);
				d = addme(d, oldd);
			}
	
			return rhex(a) + rhex(b) + rhex(c) + rhex(d);
		}
		
		
		function _utf8_encode(string) {
			
			if(!string) {
				return "";
			}
			
		    string = string.replace(/\r\n/g,"\n");
		    var utftext = "";
		
		    for (var n = 0; n < string.length; n++) {
		        var c = string.charCodeAt(n);
		
		        if (c < 128) {
		            utftext += String.fromCharCode(c);
		        }
		        else if((c > 127) && (c < 2048)) {
		            utftext += String.fromCharCode((c >> 6) | 192);
		            utftext += String.fromCharCode((c & 63) | 128);
		        }
		        else {
		            utftext += String.fromCharCode((c >> 12) | 224);
		            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
		            utftext += String.fromCharCode((c & 63) | 128);
		        }
		    }
		
		    return utftext;
		}
				
		function bitOR(a, b) {
			var lsb = (a & 0x1) | (b & 0x1);
			var msb31 = (a >>> 1) | (b >>> 1);

			return (msb31 << 1) | lsb;
		}

		function bitXOR(a, b) {			
			var lsb = (a & 0x1) ^ (b & 0x1);
			var msb31 = (a >>> 1) ^ (b >>> 1);

			return (msb31 << 1) | lsb;
		}
		
		function bitAND(a, b) { 
			var lsb = (a & 0x1) & (b & 0x1);
			var msb31 = (a >>> 1) & (b >>> 1);

			return (msb31 << 1) | lsb;
		}

		function addme(x, y) {
			var lsw = (x & 0xFFFF)+(y & 0xFFFF);
			var msw = (x >> 16)+(y >> 16)+(lsw >> 16);

			return (msw << 16) | (lsw & 0xFFFF);
		}

		function rhex(num) {
			var str = "";
			var j;

			for(j=0; j<=3; j++)
				str += hex_chr.charAt((num >> (j * 8 + 4)) & 0x0F) + hex_chr.charAt((num >> (j * 8)) & 0x0F);

			return str;
		}

		function str2blks_MD5(str) {
			var nblk = ((str.length + 8) >> 6) + 1;
			var blks = new Array(nblk * 16);
			var i;

			for(i=0; i<nblk * 16; i++) {
				blks[i] = 0;
			}
																
			for(i=0; i<str.length; i++) {
				blks[i >> 2] |= str.charCodeAt(i) << (((str.length * 8 + i) % 4) * 8);
			}

			blks[i >> 2] |= 0x80 << (((str.length * 8 + i) % 4) * 8);

			var l = str.length * 8;
			blks[nblk * 16 - 2] = (l & 0xFF);
			blks[nblk * 16 - 2] |= ((l >>> 8) & 0xFF) << 8;
			blks[nblk * 16 - 2] |= ((l >>> 16) & 0xFF) << 16;
			blks[nblk * 16 - 2] |= ((l >>> 24) & 0xFF) << 24;

			return blks;
		}
		
		function rol(num, cnt) {
			return (num << cnt) | (num >>> (32 - cnt));
		}

		function cmn(q, a, b, x, s, t) {
			return addme(rol((addme(addme(a, q), addme(x, t))), s), b);
		}

		function ff(a, b, c, d, x, s, t) {
			return cmn(bitOR(bitAND(b, c), bitAND((~b), d)), a, b, x, s, t);
		}

		function gg(a, b, c, d, x, s, t) {
			return cmn(bitOR(bitAND(b, d), bitAND(c, (~d))), a, b, x, s, t);
		}

		function hh(a, b, c, d, x, s, t) {
			return cmn(bitXOR(bitXOR(b, c), d), a, b, x, s, t);
		}

		function ii(a, b, c, d, x, s, t) {
			return cmn(bitXOR(c, bitOR(b, (~d))), a, b, x, s, t);
		}
		
	}
	encode = new Encoder();

	REQUESTS = {
		promos: {
			json: JSON.stringify({"cache":false, "section":"promos", "action":"_list", "publickey": PUBLIC_KEY}),
			data: undefined,
			hash: undefined
		},

		redeem: {
			json: JSON.stringify({"cache":false,"section":"promos","action":"redeem","publickey":"35wkye0ehviae14nxt44"}),
			data: undefined,
			hash: undefined
		},

		getVersion: {
			json: JSON.stringify({"cache":false, "section":"brands", "action":"getVersion", "publickey": PUBLIC_KEY, "brand": BRAND}),
			data: undefined,
			hash: undefined
		},

		content: {
			json: JSON.stringify({"cache":false, "section":"contents", "action":"_list", "publickey": PUBLIC_KEY}),
			data: undefined,
			hash: undefined
		},

		shops: {
			json: JSON.stringify({"cache":false, "section":"shops", "action":"_list", "publickey":PUBLIC_KEY}),
			data: undefined,
			hash: undefined
		},

		categories: {
			json: JSON.stringify({"cache":false, "section":"brands", "action":"listCategories", "publickey": PUBLIC_KEY,"brand": BRAND}),
			data: undefined,
			hash: undefined
		}
	}
	REQUESTS.promos.data = encode.base64(REQUESTS.promos.json);
	REQUESTS.redeem.data = encode.base64(REQUESTS.redeem.json);
	REQUESTS.getVersion.data = encode.base64(REQUESTS.getVersion.json);
	REQUESTS.content.data = encode.base64(REQUESTS.content.json);
	REQUESTS.shops.data = encode.base64(REQUESTS.shops.json);
	REQUESTS.categories.data = encode.base64(REQUESTS.categories.json);

	REQUESTS.promos.hash = encode.md5(REQUESTS.promos.json + "" + PRIVATE_KEY);
	REQUESTS.redeem.hash = encode.md5(REQUESTS.redeem.json + "" + PRIVATE_KEY);
	REQUESTS.getVersion.hash = encode.md5(REQUESTS.getVersion.json + "" + PRIVATE_KEY);
	REQUESTS.content.hash = encode.md5(REQUESTS.content.json + "" + PRIVATE_KEY);
	REQUESTS.shops.hash = encode.md5(REQUESTS.shops.json + "" + PRIVATE_KEY);
	REQUESTS.categories.hash = encode.md5(REQUESTS.categories.json + "" + PRIVATE_KEY);
/* Encoder */